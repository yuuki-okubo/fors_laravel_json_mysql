<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Machines;
use Faker\Generator as Faker;
$factory->define(App\Machines::class, function (Faker $faker) {
    $sampledata = [
        'temp' => $faker->numberBetween(5,35) ,//cercius
        'pressure' => $faker->randomFloat(2, 0, 100) ,//P
        'current' => $faker->randomFloat(2, 0, 100)  ,//A
        'voltage' => $faker->randomFloat(2, 0, 100) ,//V
        'flow_velocity' => $faker->randomFloat(2, 0, 100) ,//3/h
    ];
    
    return [
        //

        'name' => $faker->name,
        'data' => json_encode($sampledata,JSON_PRETTY_PRINT,JSON_NUMERIC_CHECK),
        'created_at' => $faker->datetime($max = 'now',$timezone = date_default_timezone_get()),
    ];
});
