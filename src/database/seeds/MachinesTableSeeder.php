<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Factories;
use Illuminate\Database\Eloquent\Model;


class MachinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
       factory(App\Machines::class, 50)->create();
        
    }
}
