<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Machines;

class TestController extends Controller
{
    //
    public function index(){ //darabaseの中が見れます。
        $items = DB::select('select * from machines');
        return view('test.index', ['items' => $items]);
        
    }

    public function selectdata(Request $request)//日時検索のフォームを表示
    {
        return view('test.select', ['created_at' => '']);
    }

    public function tyohyou(Request $request){ //この関数がdocurainでデータを表示させる
            
            $items = DB::table('machines')->whereDate('created_at',$request->created_at)->first();
            
            $token = 'MkWsCdxIqUcTU5CUboL6G3qe6S4l6KB5MIjv5fGB';// Docurain API トークン
            $out_type = 'pdf';           // 出力形式
            $template_name = 'yuuki_okubo_test_1'; // 保存済みテンプレート名
            $entity_json = $items->data;     // 帳票テンプレートに適用する任意のデータ（JSON）

            $url = "https://api.docurain.jp/api/{$out_type}/{$template_name}";
            $headers['Authorization'] = "token {$token}";
            $headers['Content-Type'] = 'application/json';
            $options = [
                'http_errors' => false,
                'headers'     => $headers,
                'body'        => $entity_json
            ];
           
            $client = new Client(['base_uri' => $url]);
            $path = '/index.html';
            $response = $client->request('POST', $url, $options);
            $responseBody = $response->getBody()->getContents();
           
        
            $path_parts = pathinfo($responseBody);

            header('Content-Type: application/pdf');
            echo $responseBody;
            
    }

    
}
