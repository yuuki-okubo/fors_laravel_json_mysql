<html>
    <body>
    @section('content')
        <table>
        <tr><th>Name</th><th>Data</th><th>created_at</th><th>updated_at</th></tr>
        @foreach ($items as $item)
             <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->data}}</td>
                <td>{{$item->created_at}}</td>
                <td>{{$item->updated_at}}</td>
             </tr>
         @endforeach
         </table>
         @php
         echo getcwd();
         @endphp
    @endsection    
    @yield('content') 
    </body>
</html>